<?php
namespace Omnipay\Wipay\Message;

use \Omnipay\Common\Message\AbstractResponse;
use \Omnipay\Common\Message\RedirectResponseInterface;
use Symfony\Component\HttpFoundation\RedirectResponse as HttpRedirectResponse;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class PurchaseResponse extends AbstractResponse implements RedirectResponseInterface
{
	
	const SUCCESS = 'OK';
    /**
     * Constructor
     *
     * @param RequestInterface $request the initiating request.
     * @param mixed $data
     */
    public function __construct($request, $data)
    {
    	$location = parse_url($data['url']); 
    	
    	if(!empty($location['query']))
        {
        	parse_str($location['query'], $query);
        	$data = array_merge($data,$query );
        }
    
        parent::__construct($request, $data);
    }

    public function getData() {
      return $this->data;
    }

    public function isRedirect()
    {
      return true;
    }

    public function isSuccessful(){
      return $this->getMessage() == self::SUCCESS;
    }

    public function getRedirectUrl()
    {
        return $this->getData()['url'];
    }

	public function getMessage()
    {
    	return $this->getData()['message'];
    }

	public function getStatus()
    {
    	return $this->getData()['status'];
    }

    public function getRedirectMethod()
    {
        return 'GET';
    }

    /**
    * Validate that the current Response is a valid redirect.
    *
    * @return void
    */
   protected function validateRedirect()
   {
       if (!$this instanceof RedirectResponseInterface || !$this->isRedirect()) {
           throw new RuntimeException('This response does not support redirection.');
       }
       if (empty($this->getRedirectUrl())) {
           throw new RuntimeException('The given redirectUrl cannot be empty.');
       }
       if (!in_array($this->getRedirectMethod(), ['GET', 'POST'])) {
           throw new RuntimeException('Invalid redirect method "'.$this->getRedirectMethod().'".');
       }
   }

   /**
     * Gets the redirect form data array, if the redirect method is POST.
     *
     * @return array
     */
    public function getRedirectData()
    {
      return $this->getData();
    }
}
