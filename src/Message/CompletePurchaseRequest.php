<?php

namespace Omnipay\Wipay\Message;


use Omnipay\Wipay\WipayFields;
use Omnipay\Wipay\WipayInitializeTrait;
use \Omnipay\Common\Message\AbstractRequest;
use \Omnipay\Common\Exception\InvalidRequestException;
use \Omnipay\Common\Exception\InvalidResponseException;
use Omnipay\Wipay\Message\CompletePurchaseResponse as Response;

class CompletePurchaseRequest extends AbstractRequest
{
  
  use WipayInitializeTrait, WipayFields;
  
  const SUCCESS = 'success';

  public function setCard($value)
  {
  	 
  }

  public function getStatus()
  {
    return $this->getParameter('status');
  }

  public function setStatus($value)
  {
      $this->setParameter('status', $value);
  }

	public function getDate() : string
    {
    	return $this->getParameter('date');
    }

	public function setDate(string $value)
    {
    	$this->setParameter('date', $value);
    }

	public function getHash() {
    	return $this->getParameter('hash');
    }

	public function setHash($value) {
    	return $this->setParameter('hash', $value);
    }

  	public function sendData($data)
  	{
    	return new Response($this, $data);
 	 }

   	public function getData()
    {
   	 	/*
   	     * do some validating
   	     *
   	     */
    	call_user_func_array(array($this, 'validate'), ['order_id','amount','transactionId','customer_name','status','date','currency', 'api_key']); 
    	if ((self::SUCCESS == $this->getStatus()) && !$this->validateHashKey()) {
        	throw new InvalidRequestException("The hash key is invalid");
        }

    	return $this->getParameters();
    }


	public function getCustomerName()
    {
    	return $this->getParameter('customer_name');
    }

	public function setCustomerName($value)
    {
    	$this->setParameter('customer_name', $value);
    }

	public function setMessage($value)
    {
    	$this->setParameter('message', $value);
    }

	
	public function getMessage()
    {
    	return $this->getParameter('message');
    }

	public function getApiKey()
    {
    	return $this->getParameter('api_key');
    }

	public function setApiKey($value)
    {
    	$this->setParameter('api_key', $value);
    }

	protected function validateHashKey()
  	{
    	return $this->getHash() == $this->_createHashKey();
  	}

    private function _createHashKey(){
    	return md5($this->getTransactionId()
            . $this->getTotal()
              . $this->getApiKey()
            );
   }
	
}
