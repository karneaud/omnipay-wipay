<?php

namespace Omnipay\Wipay\Message;

use Omnipay\Wipay\WipayFields as Fields;
use Omnipay\Common\Message\AbstractRequest;
use Omnipay\Common\Message\RequestInterface;
use Omnipay\Common\Exception\InvalidRequestException;
use Omnipay\Wipay\WipayFieldsValidation as Validation;
use Omnipay\Wipay\Message\PurchaseResponse as Response;

class PurchaseRequest extends AbstractRequest implements RequestInterface
{

  use Fields, Validation;
	
  protected $currencies = ['TTD','USD','JMD'];
  protected $api_url = 'https://tt.wipayfinancial.com/plugins/payments/request';

  public function sendData($data)
  {
  
  	$method = method_exists($this->httpClient, 'request') ? 'request' : 'createRequest';
  	$response = call_user_func_array(array($this->httpClient, $method),[ 'POST', $this->api_url, [
    	'content-type' => 'application/x-www-form-urlencoded',
    	'accept' => 'application/json'
    ], $method == 'request' ? ($data ? http_build_query($data) : null) : $data  ]);
  	if($method == 'createRequest') {
    	$response = $response->send();
    	$data = (array) $response->json();
    } else {
    	$data = (array) json_decode($response->getBody()->getContents());
    }
  	
    return $this->createResponse(
    	array_merge(
        	$data, 
        	['code' => $response->getStatusCode() ], 
        	(array) $response->getHeaders())
        );
  }

	protected function createResponse($data, $headers = [])
    {
        return $this->response = new Response($this, $data, $headers);
    }

  public function getData()
  {
    /*
    * do some validating
    *
    */
    call_user_func_array(array($this, 'validate'), ['account_number',
        'returnUrl',
        'amount',
      	'currency',
      	'country_code',
      	'fee_structure',
      	'paymentMethod',
      	'transactionId',
        'origin',
      	'avs' ]);
  
     switch(false) {
        	case $this->isValidMethod($this->getMethod()): throw new InvalidRequestException("Invalid Wipay method defined"); break;
        	case $this->isValidCountryCode($this->getCountryCode()): throw new InvalidRequestException("Invalid Wipay country code defined"); break;
        	case $this->isValidFeeStructure($this->getFeeStructure()): throw new InvalidRequestException("Invalid Wipay fee structure defined"); break;
        	case $this->isValidCurrencyCode($this->getCurrency()): throw new InvalidRequestException("Invalid Wipay currency code defined"); break;
        	default: break;
        }
  
   	 return [
      	'account_number' => $this->getAccountNumber(),
        'response_url' => $this->getResponseUrl(),
        'total' => $this->getTotal(),
      	'currency' => $this->getCurrency(),
      	'country_code' => $this->getCountryCode(),
      	'data' => $this->getJSONData(),
      	'environment' => $this->getEnvironment(),
      	'fee_structure' => $this->getFeeStructure(),
      	'method' => $this->getMethod(),
      	'order_id' => $this->getTransactionId(),
        'origin' => $this->getOrigin(),
      	'avs' => $this->getAVS(),
        'api_key' => $this->getApiKey()
      ]; 
  }


}
