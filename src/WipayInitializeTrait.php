<?php
namespace Omnipay\Wipay;

trait WipayInitializeTrait {

	public function initialize(array $parameters = array())
    {
        if(array_key_exists('transaction_id', $parameters)) $parameters['transactionId'] = $parameters['transaction_id'];
    
       	parent::initialize($parameters);

        return $this;
    }
	
}