<?php

namespace Omnipay\Wipay;

use \Omnipay\Common\AbstractGateway;
use Omnipay\Wipay\WipayFields as Fields;
use Omnipay\Wipay\WipayAVSFields as AVSFields;

class Gateway extends AbstractGateway
{
	use Fields;

    const ENVIRONMENT_SANDBOX = 'sandbox'; 
    const ENVIRONMENT_LIVE = 'live';

    public function getName()
    {
        return 'Wipay';
    }

    public function getDefaultParameters()
    {
        return array('account_number' => '1234567890',
                    'testMode' => false, 'avs' => 0, 'country_code' => 'TT', 'currency' => 'TTD', 'fee_structure' => 'customer_pay', 'method' => 'credit_card', 'origin' => 'Application_Name', 'api_key' => 123);
    }

    public function completePurchase(array $parameters = array())
    {
        return $this->createRequest('Omnipay\Wipay\Message\CompletePurchaseRequest', array_merge($this->getDefaultParameters(), $this->getParameters(), $parameters));
    }

	 public function purchase(array $parameters = array())
    {
        return $this->createRequest('Omnipay\Wipay\Message\PurchaseRequest', array_merge($this->getDefaultParameters(), $this->getParameters(), $parameters));
    }

	/**
     * Get the payment issuer.
     *
     * This field is used by some European gateways, which support
     * multiple payment providers with a single API.
     *
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->getParameter('paymentMethod');
    }

    /**
     * Set the payment method.
     *
     * This field is used by some European gateways, which support
     * multiple payment providers with a single API.
     *
     * @param string $value
     * @return $this
     */
    public function setPaymentMethod($value)
    {
        return $this->setParameter('paymentMethod', $value);
    }
}
