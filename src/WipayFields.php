<?php
namespace Omnipay\Wipay;

use Omnipay\Wipay\Gateway;
use Omnipay\Common\Exception;

trait WipayFields {

  public function setAccountNumber($value = '1234567890')
  {
     $this->setParameter('account_number', $value);
  }

  public function getAccountNumber()
  {
      return $this->getParameter("account_number");
  }
  
  public function setCountryCode($value = 'TT')
  {
  	$this->setParameter('country_code', $value);
  }

  public function getCountryCode()
  {
      return $this->getParameter("country_code");
  }

  public function getFeeStructure()
  {
      return $this->getParameter("fee_structure");
  }

  public function setFeeStructure($value = "customer_pay")
  {
  	  $this->setParameter('fee_structure', $value);
  }

  public function setMethod($value = 'credit_card') {
  	$this->setPaymentMethod( $value);
  }
  
  public function getMethod() {
  	 return $this->getPaymentMethod();
  }

  public function setResponseUrl($value) {
  	 $this->setReturnUrl($value);
  }
  
  public function getResponseUrl() {
  	 return $this->getReturnUrl();
  }

	public function setAVS($value = 0) {
  	 $this->setParameter("avs", $value);
  }
  
  public function getAVS() {
  	 return $this->getParameter("avs");
  }
  
	public function setOrderId($value) {
    	$this->setParameter('order_id', $value);
    }
	
	public function getOrderId() {
    	return $this->getParameter('order_id');
    }

	public function setTotal($value) {
    	$this->setAmount($value);
    }

	public function getTotal() : string {
    	return sprintf("%01.2f", $this->getParameter('amount'));
    }

	public function setData(string $value = null) {
    	return $this->setParameter('data', $value);
    }

	public function getJSONData() : ?string {
    	return $this->getParameter('data');
    }

	public function getEnvironment() {
    	return $this->getTestMode()? Gateway::ENVIRONMENT_SANDBOX : Gateway::ENVIRONMENT_LIVE;
    }

	public function setOrigin($value) {
    	$this->setParameter('origin', $value);
    }

	public function getOrigin() {
    	return $this->getParameter('origin');
    }

	public function setApiKey($value) {
    	$this->setParameter('api_key', $value);
    }

	public function getApiKey() {
    	return $this->getParameter('api_key');
    }
}

?>
