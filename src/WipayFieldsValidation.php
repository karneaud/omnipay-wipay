<?php
namespace Omnipay\Wipay;

trait WipayFieldsValidation {

	protected $payment_methods = ['credit_card', 'voucher'];
	protected $country_codes = ['TT','JM','BB'];
	protected $currency_codes = ['TTD','JMD','USD'];
	protected $fee_structures = ['merchant_absorb','split','customer_pay'];

	protected function isValidMethod($method) {
    	return in_array($method, $this->payment_methods );
    }

	protected function isValidMethodCountry($method, $country = 'TT') {
    	return $method == 'voucher' && $country == 'TT';
    }

	protected function isValidCountryCode($code) {
    	return in_array($code, $this->country_codes);
    }

	protected function isValidCurrencyCode($code) {
    	return in_array($code,$this->currency_codes );
    }
	
	protected function isValidFeeStructure($fee) {
    	return in_array($fee, $this->fee_structures);
    }
  
}

?>
